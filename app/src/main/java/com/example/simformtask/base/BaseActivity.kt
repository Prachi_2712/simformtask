package com.example.simformtask.base

import android.app.Dialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.Window
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.example.simformtask.R
import com.example.simformtask.Utils.AppUtils

open abstract class BaseActivity<T:BaseViewModel> : AppCompatActivity() {

    private var progressDialog: Dialog? = null
    private lateinit var viewModel: T

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setObservables()
    }

    abstract fun getViewModel():T


    private fun setObservables(){
        viewModel = getViewModel()
        viewModel.errorMessage.observe(this,{
            AppUtils.showToast(this,resources.getString(it))
            onApiError()
        })
        viewModel.showLoading.observe(this ,{
            if (it){
                showLoading()
            } else {
                hideLoading()
            }
        })
    }

    private fun showLoading() {
        try {
            if (progressDialog == null) {
                progressDialog = Dialog(this)
                progressDialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            }
            // inflating and setting view of custom dialog
            val view = LayoutInflater.from(this).inflate(R.layout.app_loading_dialog, null, false)
            progressDialog!!.setContentView(view)

            // setting background of dialog as transparent
            val window = progressDialog!!.window
            window?.setBackgroundDrawable(
                ContextCompat.getDrawable(
                    this,
                    android.R.color.transparent
                )
            )

            // preventing outside touch and setting cancelable false
            progressDialog!!.setCancelable(false)
            progressDialog!!.setCanceledOnTouchOutside(false)
            progressDialog!!.show()
        }catch (e: Exception){
            Log.e("Show Progress Error",e.message ?: "")
        }
    }

    private fun hideLoading() {
        try {
            progressDialog?.run {
                if (isShowing) {
                    dismiss()
                }
            }
        } catch (e:Exception){
            Log.e("Hide Progress Error",e.message ?: "")
        }
    }

    abstract fun onApiError()
}