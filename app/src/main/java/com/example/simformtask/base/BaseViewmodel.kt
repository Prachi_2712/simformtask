package com.example.simformtask.base

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.simformtask.R
import com.example.simformtask.Utils.AppUtils
import com.example.simformtask.network.ApiParams
import com.example.simformtask.network.RxCallback
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Response

open class BaseViewModel(application: Application) : AndroidViewModel(application) {

    var errorMessage: MutableLiveData<Int> = MutableLiveData()
    var showLoading: MutableLiveData<Boolean> = MutableLiveData()

    protected var subscription: Disposable? = null

    fun onApiStart(){
        showLoading.value = true
    }

    fun onApiFinish() {
        showLoading.value = false
    }

    /**
     * Used to call any API with show/hide progressbar.
     * If internet connection is available than this method will fetch data from server
     * @param api
     * @param rxCallback
     * @return Disposable
     * */
    fun <T> callRestApi (api: Observable<Response<T>>, rxCallback: RxCallback<T>?): Disposable? {
        return if (AppUtils.hasInternet(getApplication())) {
            (api)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnSubscribe{onApiStart()}
                .doOnTerminate{onApiFinish()}
                .subscribe(
                    {handleSuccessResponse(it,rxCallback)},
                    this::handleError
                )
        } else {
            interNetConnectionerror()
            null
        }
    }

    /**
     * Used to call any API without progressbar
     * If internet connection is available than this method will fetch data from server
     * @param api
     * @param rxCallback
     * @return Disposable
     * */
    fun <T> callRestApiWithoutProgress (api: Observable<Response<T>>, rxCallback:RxCallback<T>?): Disposable? {
        return if (AppUtils.hasInternet(getApplication())) {
            (api)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                    {handleSuccessResponse(it,rxCallback)},
                    this::handleError
                )
        } else {
            interNetConnectionerror()
            rxCallback?.onError()
            null
        }
    }

    /**
     * Used to handle server response
     * @param response
     * @param rxCallBack
     * */
    private fun <T> handleSuccessResponse(response: Response<T>, rxCallBack:RxCallback<T>?){
        when (response.code()) {
            ApiParams.RESPONSE_CODE_SUCCESS_200 -> {
                rxCallBack?.onSuccess(response.body() as T)
            } else ->
            rxCallBack?.onError()

        }
    }

    /**
     * Used to handle server error
     * @param error
     * */
    private fun handleError(error:Throwable){

    }

    /**
     * Used to handle internet connection error while there is no internet
     * */
    private fun interNetConnectionerror(){
        errorMessage.value = R.string.no_internet
    }

    override fun onCleared() {
        super.onCleared()
        subscription?.dispose()
    }
}