package com.example.simformtask.base

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView

class BaseBindingViewHolder: RecyclerView.ViewHolder {
    val binding: ViewDataBinding

    constructor(binding: ViewDataBinding):super(binding.root) {
        this.binding = binding
    }
}