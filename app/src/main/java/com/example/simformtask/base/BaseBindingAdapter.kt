package com.example.simformtask.base

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView

open abstract class BaseBindingAdapter<T>: RecyclerView.Adapter<BaseBindingViewHolder>() {
    protected var items: ArrayList<T> = ArrayList<T>()

    override fun getItemCount() = items.count()

    /**
     * Use to add items in adapter
     * @param items
     * */
    fun setItem(items:ArrayList<T>){
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    /**
     * Used delete all data from adapter
     * */
    fun clear(){
        val size = items.size
        if (items.isNotEmpty()){
            items.clear()
            notifyItemRangeRemoved(0,size)
        }
    }

    /**
     * Use to add list of items in adapter
     * @param items
     * */
    fun addItems(items:ArrayList<T>){
        val size = this.items.size
        this.items.addAll(items)
        val newSize = this.items.size
        notifyItemRangeInserted(size,newSize)
    }

    /**
     * Check if adapter is having any data or not
     * */
    fun isAdapterEmpty():Boolean = items.isEmpty()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseBindingViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = bind(inflater,parent,viewType)
        return BaseBindingViewHolder(binding)
    }

    abstract fun bind(inflator: LayoutInflater, parent: ViewGroup, viewType:Int): ViewDataBinding
}