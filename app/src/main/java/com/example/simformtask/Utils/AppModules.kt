package com.example.simformtask.Utils

import android.content.Context
import com.example.simformtask.network.ApiParams
import com.example.simformtask.network.ApiService
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import java.util.concurrent.TimeUnit

object AppModules {
    private lateinit var prefs:Prefs
    private lateinit var okHttpClient: OkHttpClient
    private lateinit var apiService: ApiService

    fun providePrefs(context: Context):Prefs {
        if (!this::prefs.isInitialized) {
            prefs = Prefs.getInstance(context)
        }
        return prefs
    }

    private fun provideOkHttpClient(): OkHttpClient {
        if (!this::okHttpClient.isInitialized) {
            val time = 100
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            okHttpClient = OkHttpClient().newBuilder()
                .connectTimeout(time.toLong(), TimeUnit.SECONDS)
                .readTimeout(time.toLong(), TimeUnit.SECONDS)
                .writeTimeout(time.toLong(), TimeUnit.SECONDS)
                .addInterceptor(loggingInterceptor)
                .build()
        }
        return okHttpClient
    }

    fun provideApiService() : ApiService {
        if (!this::apiService.isInitialized) {
            apiService = Retrofit.Builder()
                .baseUrl(ApiParams.BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .addConverterFactory(GsonConverterFactory.create())
                .client(provideOkHttpClient())
                .build()
                .create()
        }
        return apiService
    }
}