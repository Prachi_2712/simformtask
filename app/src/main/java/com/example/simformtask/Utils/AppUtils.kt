package com.example.simformtask.Utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.widget.Toast

object AppUtils {

    /**
     * Used to check if device is having any working internet connection or not
     * @param context
     * @return Boolean
     * */
    fun hasInternet(context: Context):Boolean{
        val connectiivtyManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val nw = connectiivtyManager.activeNetwork ?: return false
        val activeNetwork = connectiivtyManager.getNetworkCapabilities(nw) ?: return  false
        return when {
            activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
            activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
            activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
            else -> false
        }
    }


    /**
     * Used to show Toast
     * @param context
     * @param message
     * */
    fun showToast(context: Context, message:String){
        Toast.makeText(context,message, Toast.LENGTH_LONG).show()
    }
}