package com.example.simformtask.Utils

import android.content.Context
import android.content.SharedPreferences

class Prefs(context: Context) {
    var preferance: SharedPreferences
    var editor: SharedPreferences.Editor

    val TAG = "My_Pref"

    init {
        preferance = context.getSharedPreferences(TAG, Context.MODE_PRIVATE)
        editor = preferance.edit()
    }

    companion object {
        private var prefs:Prefs? = null
        fun getInstance(context: Context):Prefs {
            if (prefs == null) {
                prefs = Prefs(context)
            }
            return prefs!!
        }
    }
}