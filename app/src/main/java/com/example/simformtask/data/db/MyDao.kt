package com.example.simformtask.data.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.simformtask.data.User

@Dao
interface MyDao {
    @Query("SELECT * FROM  tbl_user")
    fun getAllJobOrder(): List<User>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllUser(users:List<User>)

    @Query("DELETE FROM tbl_user")
    fun deleteAllJobOrder()

    @Query("SELECT COUNT(*) FROM tbl_user")
    fun getUserCount():Int
}