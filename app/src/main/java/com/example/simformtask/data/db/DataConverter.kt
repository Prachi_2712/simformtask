package com.example.simformtask.data.db

import androidx.room.TypeConverter
import com.example.simformtask.data.User
import com.google.gson.Gson

class DataConverter {

    @TypeConverter
    fun nameJsonToObject(value: String) = Gson().fromJson(value, User.Name::class.java)

    @TypeConverter
    fun nameObjectToJson(value: User.Name) = Gson().toJson(value)

    @TypeConverter
    fun locationJsonToObject(value: String) = Gson().fromJson(value, User.Location::class.java)

    @TypeConverter
    fun locationObjectToJson(value: User.Location) = Gson().toJson(value)

    @TypeConverter
    fun loginJsonToObject(value: String) = Gson().fromJson(value, User.Login::class.java)

    @TypeConverter
    fun loginObjectToJson(value: User.Login) = Gson().toJson(value)

    @TypeConverter
    fun dobJsonToObject(value: String) = Gson().fromJson(value, User.Dob::class.java)

    @TypeConverter
    fun dobObjectToJson(value: User.Dob) = Gson().toJson(value)

    @TypeConverter
    fun registeredJsonToObject(value: String) = Gson().fromJson(value, User.Registered::class.java)

    @TypeConverter
    fun registeredObjectToJson(value: User.Registered) = Gson().toJson(value)

    @TypeConverter
    fun idJsonToObject(value: String) = Gson().fromJson(value, User.Id::class.java)

    @TypeConverter
    fun idObjectToJson(value: User.Id) = Gson().toJson(value)

    @TypeConverter
    fun pictureJsonToObject(value: String) = Gson().fromJson(value, User.Picture::class.java)

    @TypeConverter
    fun pictureObjectToJson(value: User.Picture) = Gson().toJson(value)

    @TypeConverter
    fun streetJsonToObject(value: String) = Gson().fromJson(value, User.Location.Street::class.java)

    @TypeConverter
    fun streetObjectToJson(value: User.Location.Street) = Gson().toJson(value)

    @TypeConverter
    fun coordinatedObjectToJson(value: User.Location.Coordinates) = Gson().toJson(value)

    @TypeConverter
    fun coordinatesJsonToObject(value: String) = Gson().fromJson(value, User.Location.Coordinates::class.java)

    @TypeConverter
    fun timezoneJsonToObject(value: String) = Gson().fromJson(value, User.Location.TimeZone::class.java)

    @TypeConverter
    fun timezoneObjectToJson(value: User.Location.TimeZone) = Gson().toJson(value)
}