package com.example.simformtask.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponseUser (
    val results:ArrayList<User>?
        ):Parcelable {
}
