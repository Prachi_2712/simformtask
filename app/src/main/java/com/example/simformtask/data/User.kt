package com.example.simformtask.data

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverter
import androidx.room.TypeConverters
import com.example.simformtask.data.db.DataConverter
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "tbl_user")
@Parcelize
data class User(
    @PrimaryKey(autoGenerate = true)
    var uniqueId:Int,
    val gender:String?,
    @TypeConverters(DataConverter::class)
    val name: Name?,
    @TypeConverters(DataConverter::class)
    val location:Location?,
    val email:String?,
    @TypeConverters(DataConverter::class)
    val login:Login?,
    @TypeConverters(DataConverter::class)
    val dob:Dob,
    @TypeConverters(DataConverter::class)
    val registered:Registered?,
    val phone:String?,
    val cell:String?,
    @TypeConverters(DataConverter::class)
    val id:Id?,
    @TypeConverters(DataConverter::class)
    val picture:Picture?,
    val nat:String?
): Parcelable {
    @Parcelize
    class Name(
        val title:String?,
        val first:String?,
        val last:String?
    ): Parcelable {

    }
    @Parcelize
    data class Location(
        @TypeConverters(DataConverter::class)
        val street:Street?,
        val city:String?,
        val state:String?,
        val country:String?,
        val postcode:String?,
        @TypeConverters(DataConverter::class)
        val coordinates:Coordinates?,
        @TypeConverters(DataConverter::class)
        val timezone:TimeZone?
    ): Parcelable {
        @Parcelize
        data class Street(
            val number:Int?,
            val name:String?
        ): Parcelable {

        }
        @Parcelize
        data class Coordinates(
            val latitude:String?,
            val longitude:String?
        ): Parcelable {

        }
        @Parcelize
        data class TimeZone(
            val offset:String?,
            val description:String?
        ): Parcelable {

        }
    }

    @Parcelize
    data class Login(
        val uuid:String?,
        val username:String?,
        val password:String?,
        val salt:String?,
        val md5:String?,
        val sha1:String?,
        val sha256:String?
    ): Parcelable {

    }

    @Parcelize
    data class Dob(
        val date:String?,
        val age:Int?
    ): Parcelable {

    }

    @Parcelize
    data class Registered(
        val date:String?,
        val age:Int?
    ): Parcelable {

    }

    @Parcelize
    data class Id(
        val name:String?,
        val value:String?
    ): Parcelable {

    }

    @Parcelize
    data class Picture(
        val large:String?,
        val medium:String?,
        val thumbnail:String?
    ): Parcelable {

    }
}