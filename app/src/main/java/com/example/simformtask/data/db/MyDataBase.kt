package com.example.simformtask.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.simformtask.data.User

@Database(entities = [User::class],
version = 1,
exportSchema = false)
@TypeConverters(DataConverter::class)
public abstract class MyDataBase : RoomDatabase() {
    abstract fun getDao():MyDao

    companion object {
        @Volatile
        private var INSTANCE: MyDataBase? = null
        fun getDatabase(context: Context): MyDataBase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                try {
                    val instance = Room.databaseBuilder(
                        context.applicationContext,
                        MyDataBase::class.java,
                        "my_database.db"
                    ).allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                        .build()
                    INSTANCE = instance
                    return instance
                }catch (e:Exception) {
                    val instance = Room.databaseBuilder(
                        context.applicationContext,
                        MyDataBase::class.java,
                        "my_database.db"
                    ).allowMainThreadQueries()
                        .fallbackToDestructiveMigration()
                        .build()
                    INSTANCE = instance
                    return instance
                }
            }
        }
    }
}