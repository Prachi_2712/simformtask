package com.example.simformtask.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.simformtask.R
import com.example.simformtask.base.BaseBindingAdapter
import com.example.simformtask.base.BaseBindingViewHolder
import com.example.simformtask.data.ResponseUser
import com.example.simformtask.data.User
import com.example.simformtask.databinding.UserItemBinding

class UserAdapter:BaseBindingAdapter<User>() {
    override fun bind(inflator: LayoutInflater, parent: ViewGroup, viewType: Int): ViewDataBinding {
        return UserItemBinding.inflate(inflator,parent,false)
    }

    override fun onBindViewHolder(holder: BaseBindingViewHolder, position: Int) {
        val binding = holder.binding as UserItemBinding
        val item = items[position]

        binding.user = item

        Glide.with(holder.itemView.context)
            .load(item.picture?.medium)
            .diskCacheStrategy(DiskCacheStrategy.DATA)
            .placeholder(R.drawable.ic_user)
            .into(binding.imgUser)
    }
}