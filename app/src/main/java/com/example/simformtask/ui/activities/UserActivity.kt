package com.example.simformtask.ui.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import com.example.simformtask.R
import com.example.simformtask.Utils.AppModules
import com.example.simformtask.base.BaseActivity
import com.example.simformtask.databinding.ActivityMainBinding
import com.example.simformtask.ui.adapter.UserAdapter
import com.example.simformtask.viewmodel.MyViewModelFactory
import com.example.simformtask.viewmodel.UserViewModel

class UserActivity : BaseActivity<UserViewModel>() {

    private lateinit var binding:ActivityMainBinding

    private lateinit var adapter:UserAdapter

    private val userViewModel: UserViewModel by viewModels<UserViewModel>{
        viewmodelFactory
    }

    lateinit var viewmodelFactory: MyViewModelFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        viewmodelFactory = MyViewModelFactory(application, AppModules.provideApiService(),AppModules.providePrefs(this))
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_main)
        init()
    }

    private fun init(){
        adapter = UserAdapter()
        setObservable()
        getUserList()
        binding.swipeLayout.setOnRefreshListener { 
            getUserList()
        }

        binding.btnRetry.setOnClickListener {
            getUserList()
        }
    }

    private fun setObservable(){
        userViewModel.getUserListResponse().observe(
            { this.lifecycle },
            {
                binding.swipeLayout.isRefreshing = false
                adapter.clear()
                adapter.setItem(it)
                binding.recUser.adapter = adapter

                binding.swipeLayout.visibility = View.VISIBLE
                binding.grpNoData.visibility = View.GONE
            }
        )
    }

    private fun getUserList(){
        userViewModel.getUserList()
    }

    override fun getViewModel(): UserViewModel = userViewModel

    override fun onApiError() {
        if (this::adapter.isInitialized && adapter.isAdapterEmpty()) {
            binding.swipeLayout.visibility = View.GONE
            binding.grpNoData.visibility = View.VISIBLE
        }
    }
}