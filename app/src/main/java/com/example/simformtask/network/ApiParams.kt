package com.example.simformtask.network

object ApiParams {
    const val BASE_URL = "https://randomuser.me/"
    const val GET_USER_LIST = "api"
    const val RESPONSE_CODE_SUCCESS_200 = 200

    const val RESULTS = "results"
}