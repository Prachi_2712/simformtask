package com.example.simformtask.network

interface RxCallback<T> {
    fun onSuccess(t:T)
    fun onError()
}