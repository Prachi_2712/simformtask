package com.example.simformtask.network

import com.example.simformtask.data.ResponseUser
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface ApiService {
    @GET(ApiParams.GET_USER_LIST)
    fun getUserList(@QueryMap params:HashMap<String,String>): Observable<Response<ResponseUser>>
}