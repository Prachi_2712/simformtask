package com.example.simformtask.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.simformtask.R
import com.example.simformtask.Utils.AppUtils
import com.example.simformtask.data.ResponseUser
import com.example.simformtask.Utils.Prefs
import com.example.simformtask.base.BaseViewModel
import com.example.simformtask.data.User
import com.example.simformtask.data.db.MyDao
import com.example.simformtask.data.db.MyDataBase
import com.example.simformtask.network.ApiParams
import com.example.simformtask.network.ApiService
import com.example.simformtask.network.RxCallback
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class UserViewModel(
    application: Application,
    val apiService: ApiService,
    val prefs: Prefs
): BaseViewModel(application) {

    private val dao : MyDao by lazy {
        MyDataBase.getDatabase(application).getDao()
    }

    private val userListResponse: MutableLiveData<ArrayList<User>> by lazy {
        MutableLiveData<ArrayList<User>>()
    }

    fun getUserListResponse(): LiveData<ArrayList<User>> = userListResponse

    /**
     * Used to get user list from room DB and call server request
     * */
    fun getUserList(){
        Observable.just(
            dao.getAllJobOrder()
        ).map {
            if (!it.isNullOrEmpty()){
                var arrayListOfUser = arrayListOf<User>()
                arrayListOfUser.addAll(it)
                userListResponse.postValue(arrayListOfUser)
            }
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { onApiStart() }
            .doOnTerminate { onApiFinish() }
            .subscribe()
        getUserListFromServer()

    }


    /**
     * Used to get user list from server
     * */
    private fun getUserListFromServer(){
        val params = HashMap<String,String>()
        params[ApiParams.RESULTS] = 100.toString()

        if (dao.getUserCount() > 0) {
            subscription = callRestApiWithoutProgress(apiService.getUserList(params),
                    object : RxCallback<ResponseUser> {
                        override fun onSuccess(t: ResponseUser) {
                            handleUserResponse(t)
                        }

                        override fun onError() {
                            errorMessage.value = R.string.error_in_api
                        }

                    })
        } else {
            subscription = callRestApi(apiService.getUserList(params),
                    object : RxCallback<ResponseUser> {
                        override fun onSuccess(t: ResponseUser) {
                            handleUserResponse(t)
                        }

                        override fun onError() {
                            errorMessage.value = R.string.error_in_api
                        }

                    })
        }
    }

    fun handleUserResponse(userResponse: ResponseUser){
        userListResponse.value = userResponse.results
        dao.deleteAllJobOrder()
        if (!userResponse.results.isNullOrEmpty()) {
            dao.insertAllUser(userResponse.results.toList())
        }
    }
}