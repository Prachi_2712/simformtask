package com.example.simformtask.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.simformtask.Utils.Prefs
import com.example.simformtask.network.ApiService
import java.lang.IllegalArgumentException

class MyViewModelFactory(
    private val application: Application,
    private val apiService: ApiService,
    private val prefs: Prefs
): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(UserViewModel::class.java)){
            return UserViewModel(application,apiService,prefs) as T
        }
        throw  IllegalArgumentException("Unknown class excaption")
    }
}